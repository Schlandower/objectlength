/*'use strict';
function length(obj) {
  return Object.keys(obj).length;
}
module.exports = length;*/
if(!Object.prototype.length) {
  Object.prototype.length = function() {
	if(typeof this !== 'object') {
	  throw new TypeError(this + ' is not an object');
	}
	return Object.keys(this).length;
  };
}
